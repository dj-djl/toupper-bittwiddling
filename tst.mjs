import fs from 'fs';
const testStrings = fs.readFileSync('input.txt', 'utf8').split('\n');
function toUpperBitTwiddling7(d) {
    let out='';
    for (let i=0;i<d.length;i++) {
        const ch = d.charCodeAt(i);
        out+=String.fromCharCode(ch&~((ch&0x40)>>1));
    }
    return out;
}

function toUpperBitTwiddling8(d) {
    let out='';
    for (let i=0;i<d.length;i++) {
        const ch = d.charCodeAt(i);
        out+=String.fromCharCode(ch&~((ch&0x40 & ~(ch&0x80)>>1)>>1));
    }
    return out;
}


function toUpperLibrary(d) {
    let out='';
    for (let i=0;i<d.length;i++) {
        out+=d[i].toUpperCase();
    }
    return out;
}

// console.log(toUpperBitTwiddling8(testStrings[0]));
// console.log(testStrings[0].toUpperCase());
// process.exit(0); 


console.time('bit twiddling7');
for (const line of testStrings) {
    toUpperBitTwiddling7(line);
} 
console.timeEnd('bit twiddling7');

console.time('bit twiddling8');
for (const line of testStrings) {
    toUpperBitTwiddling8(line);
} 
console.timeEnd('bit twiddling8');

console.time('library loop');
for (const line of testStrings) {
    toUpperLibrary(line);
} 
console.timeEnd('library loop');

console.time('library');
for (const line of testStrings) {
    line.toUpperCase();
} 
console.timeEnd('library');