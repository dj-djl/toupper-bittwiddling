# README #

This is just my attempt at a branchless bit-twiddling implementation of toUpper.

Do not use this in any serious application

To run it just do `node --experimental-modules tst.mjs`

See https://www.youtube.com/watch?v=bVJ-mWWL7cE&t=901s for the inspiration for this.

## 7 bit Explaination ##

In English 7bit ascii you can tell if the character is a letter by examining the `0x40` bit, if its set then its a letter, if its not then it isn't.

If it is indeed a letter then the `0x20` bit tells us if its upper or lower case - 0 for upper, 1 for lower.

We can use these 2 facts to our advantage.

So we can mask out to keep just the `0x40` bit and shift it to the right one position.

Then we flip all the bits.

If we started with a letter then this leaves us with `0b11011111`

At this point the original state of the `0x20` upper/lower case bit is actually irrelevant, we just want to turn it off
So if we `AND` the original value with our new mask, we can achieve that.


## 8 bit Explaination ##

For 8 bits its a little more complex, but not much.

We just need to add an additional check to ensure we're not dealing with the upper half.

This is achieved in much the same way - take the `0x80` bit, shift it to the right and flip everything, then `AND` this with the mask above (before shifting).

If the high bit was set, the net effect is to leave the `0x20` bit in our final mask on, if it was unset then it has no effect on the other logic.

## Results ##

bit twiddling7: 20.112ms
bit twiddling8: 27.688ms
library loop: 88.371ms
library: 13.743ms

If run this code yourself, your results may vary.

## Why is the library loop version so slow? ##

I have no idea. At worst I thought it would have similar performance to my 8-bit version, but its actually far worse even than that.

Perhaps because its actually using a far more sofisticated determination of what is a letter, and what lower-case and upper-case letters are.

Don't forget its dealing with the entire unicode plane, not just the English ascii characters. And there may not be such easy to deal with patterns on some of the unicode planes? I have no idea.



## Why is the library version so much faster? ##



Probably the optimisation is in the looping itself, which I made no attempt to optimise here.

Modern processors can deal with multiple operations at once, even without threading or forking.



You could probably construct a version of my implementation which could do 8 characters at a time (64 bits) if you could be bothered to deal with the edge cases, which I can't



## Conclusion ##



Don't micro-optimise! Especially not in high-level languages.

Use the built-in library functions, and use them properly, trust that your compiler knows what its doing.

You really only need to deal with this kind of thing if you are writing that library or compiler, or are dealing with assembly for some low-spec embedded CPU.



I already knew this before writing the code and I did it just for fun. **I would never use this code in any serious application, and neither should you.**

